import {NgElementConstructor} from '@angular/elements';

import {createCustomIvyElement} from './create-custom-ivy-element';
import {ChartsComponent} from './charts.component';

export const ChartsElement: NgElementConstructor<ChartsComponent> =
  createCustomIvyElement(ChartsComponent);
