import {ChangeDetectionStrategy, Component, Input, NgModule, OnInit, ViewEncapsulation} from '@angular/core';
import {ChartOptions, ChartType} from 'chart.js';
import {ChartsModule, Color, Label, ThemeService} from "ng2-charts";
import {BehaviorSubject} from "rxjs";

@Component({
  encapsulation: ViewEncapsulation.ShadowDom,
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'charts-widget',
  template: `
      <div class="charts-wrapper">
          <canvas baseChart
                  [datasets]="dataSets"
                  [labels]="chartLabels"
                  [options]="chartOptions"
                  [colors]="chartColors"
                  [legend]="chartLegend"
                  [title]="chartTitle"
                  [chartType]="chartType"
                  [plugins]="chartPlugins">
          </canvas>
      </div>
  `,
  providers: [ThemeService]
})
export class ChartsComponent implements OnInit {
  @Input() dataSets = [{data: [], label: ''}];
  @Input() chartLabels: Label[] = [];
  @Input() chartOptions: ChartOptions = {
    responsive: true,
    legend: {labels: {fontColor: '#000', fontSize: 14}, align: "end", position: "right"},
    scales: {
      xAxes: [{gridLines: {color: 'rgba(0,0,0,0.06)'}, ticks: {fontColor: '#000'}}],
      yAxes: [{gridLines: {color: 'rgba(0,0,0,0.06)'}, ticks: {fontColor: '#000'}}]
    }
  };
  @Input() chartColors: Color[] = [];
  @Input() chartLegend = true;
  @Input() chartPlugins = [];
  @Input() chartType: ChartType = 'line';
  @Input() chartTitle: string = '';

  @Input() themeMode: BehaviorSubject<string>;

  constructor(private themeService: ThemeService) {
  }

  ngOnInit(): void {
    if (this.themeMode) {
      this.themeMode.subscribe(value => {
        if (value == 'lightmode') {
          this.chartOptions.legend.labels.fontColor = '#000';
          this.chartOptions.scales.yAxes.forEach(axe => {
            axe.ticks.fontColor = '#000';
            axe.gridLines.color = 'rgba(0, 0, 0, 0.06)';
          });
          this.chartOptions.scales.xAxes.forEach(axe => {
            axe.ticks.fontColor = '#000';
            axe.gridLines.color = 'rgba(0, 0, 0, 0.06)';
          });
        } else {
          this.chartOptions.legend.labels.fontColor = '#FFF';
          this.chartOptions.scales.yAxes.forEach(axe => {
            axe.ticks.fontColor = '#FFF';
            axe.gridLines.color = 'rgba(255,255,255,0.41)';
          });
          this.chartOptions.scales.xAxes.forEach(axe => {
            axe.ticks.fontColor = '#FFF';
            axe.gridLines.color = 'rgba(255,255,255,0.4)';
          });
          this.chartOptions.scales.yAxes.forEach(axe => axe.ticks.fontColor = '#FFF');
          this.chartOptions.scales.xAxes.forEach(axe => axe.ticks.fontColor = '#FFF');
        }
        this.themeService.setColorschemesOptions(this.chartOptions);
      });
    }
  }
}

@NgModule({
  imports: [
    ChartsModule
  ],
  declarations: [
    ChartsComponent
  ]
})
class ChartWidgetModule {
}
