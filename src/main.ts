import {enableProdMode} from '@angular/core';
import {ChartsElement} from './app/charts.element';
enableProdMode();
customElements.define('charts-widget', ChartsElement);
